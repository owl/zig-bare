const bare = @import("bare");
const io = std.io;
const mem = std.mem;
const testing = std.testing;
const std = @import("std");
const time = std.time;
const Timer = time.Timer;

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();
    const tp = try benchmarkRead(allocator, 10000000);
    const stdout = std.io.getStdOut().writer();
    try stdout.print("throughput: {}\n", .{tp});
}

fn benchmarkRead(allocator: std.mem.Allocator, iterations: usize) !u64 {
    const Foo = struct {
        a: f32,
        b: u8,
        c: bool,
    };
    var timer = try Timer.start();
    const start = timer.lap();
    var fbs = io.fixedBufferStream("\x00\x00\x28\x42\x2b\x00");
    const fbs_reader = fbs.reader();
    var bd = bare.decoder(allocator, fbs_reader);

    for (0..iterations) |_| {
        const res = try bd.decode(Foo);
        mem.doNotOptimizeAway(&res);
        try fbs.seekTo(0);
    }

    const end = timer.read();
    const elapsed_s = @as(f64, @floatFromInt(end - start)) / time.ns_per_s;
    const throughput: u64 = @intFromFloat(@sizeOf(Foo) * @as(f64, @floatFromInt(iterations)) / elapsed_s);

    return throughput;
}
