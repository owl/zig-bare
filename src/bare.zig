const std = @import("std");
const varint = @import("varint");
const io = std.io;
const math = std.math;
const mem = std.mem;
const meta = std.meta;

pub const DecodeError = error{
    DuplicateKey,
    Overflow,
    InvalidBool,
    InvalidUnion,
};

pub fn decoder(allocator: mem.Allocator, reader: anytype) Decoder(@TypeOf(reader)) {
    return Decoder(@TypeOf(reader)).init(allocator, reader);
}

/// Decodes BARE messages.
/// Uses an internal arena backed by the passed-in allocator.
pub fn Decoder(comptime ReaderType: type) type {
    return struct {
        arena: std.heap.ArenaAllocator,
        reader: ReaderType,
        const Self = @This();

        /// Call `deinit` when done.
        pub fn init(allocator: mem.Allocator, reader: ReaderType) Self {
            return .{
                .arena = std.heap.ArenaAllocator.init(allocator),
                .reader = reader,
            };
        }

        /// Free everything in the internal arena allocator.
        pub fn deinit(self: *Self) void {
            self.arena.deinit();
        }

        /// Decode a supported BARE type.
        pub fn decode(self: *Self, comptime T: type) !T {
            return switch (@typeInfo(T)) {
                .int => self.decodeInt(T),
                .float => self.decodeFloat(T),
                .bool => self.decodeBool(),
                .@"struct" => if (comptime isHashMap(T))
                    self.decodeHashMap(T)
                else
                    self.decodeStruct(T),
                .@"enum" => self.decodeEnum(T),
                .optional => self.decodeOptional(T),
                .array => self.decodeArray(T),
                .@"union" => self.decodeUnion(T),
                .pointer => self.decodePointer(T),
                else => @compileError("unsupported type " ++ @typeName(T)),
            };
        }

        fn decodeAllowVoid(self: *Self, comptime T: type) !T {
            return switch (T) {
                void => {},
                else => self.decode(T),
            };
        }

        fn decodeVarInt(self: *Self, comptime T: type) !T {
            return @intCast(try varint.decodeVarInt(self.reader));
        }

        fn decodeVarUint(self: *Self, comptime T: type) !T {
            return @intCast(try varint.decodeVarUint(self.reader));
        }

        fn decodeInt(self: *Self, comptime T: type) !T {
            return switch (T) {
                usize, c_uint => self.decodeVarUint(T),
                isize, c_int => self.decodeVarInt(T),
                i8,
                u8,
                i16,
                u16,
                i32,
                u32,
                i64,
                u64,
                => self.reader.readInt(T, .little),
                else => @compileError("unsupported integer type " ++ @typeName(T)),
            };
        }

        fn decodeFloat(self: *Self, comptime T: type) !T {
            return switch (T) {
                f32, f64 => @bitCast(try self.reader.readInt(
                    meta.Int(.unsigned, @bitSizeOf(T)),
                    .little,
                )),
                else => @compileError("unsupported float type " ++ @typeName(T)),
            };
        }

        fn decodeBool(self: *Self) !bool {
            return switch (try self.reader.readByte()) {
                0 => false,
                1 => true,
                else => DecodeError.InvalidBool,
            };
        }

        fn decodeStruct(self: *Self, comptime T: type) !T {
            const ti = @typeInfo(T).@"struct";
            if (ti.fields.len < 1)
                @compileError("structs must have 1 or more fields");

            var s: T = undefined;

            inline for (ti.fields) |f|
                @field(s, f.name) = try self.decode(f.type);

            return s;
        }

        fn decodeEnum(self: *Self, comptime T: type) !T {
            const x: meta.Tag(T) = @intCast(try self.decodeVarUint(usize));
            return meta.intToEnum(T, x);
        }

        fn decodeOptional(self: *Self, comptime T: type) !T {
            if (0x0 == try self.reader.readByte())
                return null;

            return try self.decode(@typeInfo(T).optional.child);
        }

        fn decodeArray(self: *Self, comptime T: type) !T {
            const ti = @typeInfo(T).array;
            if (ti.len < 1)
                @compileError("array length must be at least 1");

            var buf: [ti.len]ti.child = undefined;

            for (0..ti.len) |i|
                buf[i] = try self.decode(ti.child);

            return buf;
        }

        fn decodeUnion(self: *Self, comptime T: type) !T {
            const ti = @typeInfo(T).@"union";

            if (ti.tag_type == null)
                @compileError("only tagged unions are supported");

            const tag = try self.decodeVarUint(usize);

            inline for (ti.fields) |f| {
                if (tag == @intFromEnum(@field(ti.tag_type.?, f.name))) {
                    const v = try self.decodeAllowVoid(f.type);
                    return @unionInit(T, f.name, v);
                }
            }

            return DecodeError.InvalidUnion;
        }

        fn decodePointer(self: *Self, comptime T: type) !T {
            const ti = @typeInfo(T).pointer;

            if (ti.size != .Slice)
                @compileError("slices are the only supported pointer type");

            const len = try self.decodeVarUint(usize);
            var buf = try self.arena.allocator().alloc(ti.child, len);

            for (0..len) |i|
                buf[i] = try self.decode(ti.child);

            return buf;
        }

        fn decodeHashMap(self: *Self, comptime T: type) !T {
            const K = HashMapKeyType(T);
            const V = HashMapValueType(T);

            if (comptime !isValidHashMapKeyType(K))
                @compileError("unsupported hashmap key type " ++ @typeName(K));

            const i = try self.decodeVarUint(usize);

            if (std.math.maxInt(T.Size) < i)
                return DecodeError.Overflow;

            var map = T.init(self.arena.allocator());
            try map.ensureUnusedCapacity(@intCast(i));

            for (0..i) |_| {
                const key = try self.decode(K);
                const val = try self.decode(V);

                const res = map.getOrPutAssumeCapacity(key);

                if (res.found_existing)
                    return DecodeError.DuplicateKey;

                res.value_ptr.* = val;
            }

            return map;
        }
    };
}

pub fn encoder(writer: anytype) Encoder(@TypeOf(writer)) {
    return Encoder(@TypeOf(writer)).init(writer);
}

/// Encodes a BARE message.
pub fn Encoder(comptime WriterType: type) type {
    return struct {
        writer: WriterType,
        const Self = @This();

        /// No `deinit` needed.
        pub fn init(writer: WriterType) Self {
            return .{
                .writer = writer,
            };
        }

        /// Encode a supported BARE type.
        pub fn encode(self: *Self, value: anytype) !void {
            const T = @TypeOf(value);
            return switch (@typeInfo(T)) {
                .int => self.encodeInt(T, value),
                .float => self.encodeFloat(T, value),
                .bool => self.encodeBool(value),
                .@"struct" => if (comptime isHashMap(T))
                    self.encodeHashMap(value)
                else
                    self.encodeStruct(value),
                .@"enum" => self.encodeEnum(value),
                .optional => self.encodeOptional(value),
                .array => self.encodeArray(value),
                .@"union" => self.encodeUnion(value),
                .pointer => self.encodePointer(value),
                else => @compileError("unsupported type " ++ @typeName(T)),
            };
        }

        fn encodeAllowVoid(self: *Self, value: anytype) !void {
            return switch (@TypeOf(value)) {
                void => {},
                else => self.encode(value),
            };
        }

        fn encodeVarUint(self: *Self, value: anytype) !void {
            return varint.encodeVarUint(self.writer, @intCast(value));
        }

        fn encodeVarInt(self: *Self, value: anytype) !void {
            return varint.encodeVarInt(self.writer, @intCast(value));
        }

        fn encodeInt(self: *Self, comptime T: type, value: T) !void {
            return switch (T) {
                usize, c_uint => self.encodeVarUint(value),
                isize, c_int => self.encodeVarInt(value),
                i8,
                u8,
                i16,
                u16,
                i32,
                u32,
                i64,
                u64,
                => self.writer.writeInt(T, value, .little),
                else => @compileError("unsupported integer type " ++ @typeName(T)),
            };
        }

        fn encodeFloat(self: *Self, comptime T: type, value: T) !void {
            const I = switch (T) {
                f32 => u32,
                f64 => u64,
                else => @compileError("unsupported float type " ++ @typeName(T)),
            };
            const x: I = @bitCast(value);
            try self.writer.writeInt(I, x, .little);
        }

        fn encodeBool(self: *Self, value: bool) !void {
            try self.writer.writeByte(@intFromBool(value));
        }

        fn encodeStruct(self: *Self, value: anytype) !void {
            const ti = @typeInfo(@TypeOf(value)).@"struct";

            if (ti.fields.len < 1)
                @compileError("structs must have 1 or more fields");

            inline for (ti.fields) |f|
                try self.encode(@field(value, f.name));
        }

        fn encodeEnum(self: *Self, value: anytype) !void {
            try self.encodeVarUint(@intFromEnum(value));
        }

        fn encodeOptional(self: *Self, value: anytype) !void {
            if (value) |val| {
                try self.writer.writeByte(@intFromBool(true));
                try self.encode(val);
            } else try self.writer.writeByte(@intFromBool(false));
        }

        fn encodeArray(self: *Self, value: anytype) !void {
            const ti = @typeInfo(@TypeOf(value)).array;

            if (ti.len < 1)
                @compileError("array length must be at least 1");

            for (value) |v|
                try self.encode(v);
        }

        fn encodePointer(self: *Self, value: anytype) !void {
            const ti = @typeInfo(@TypeOf(value)).pointer;

            if (ti.size != .Slice)
                @compileError("slices are the only supported pointer type");

            try self.encodeVarUint(value.len);

            for (value) |v|
                try self.encode(v);
        }

        fn encodeHashMap(self: *Self, value: anytype) !void {
            const T = @TypeOf(value);
            const K = HashMapKeyType(T);

            if (comptime !isValidHashMapKeyType(K))
                @compileError("unsupported hashmap key type " ++ @typeName(K));

            try self.encodeVarUint(value.count());

            var it = value.iterator();

            while (it.next()) |kv| {
                try self.encode(kv.key_ptr.*);
                try self.encode(kv.value_ptr.*);
            }
        }

        fn encodeUnion(self: *Self, value: anytype) !void {
            const T = @TypeOf(value);
            const ti = @typeInfo(T).@"union";

            if (ti.tag_type) |TT| {
                const tag = @intFromEnum(value);
                try self.encodeVarUint(tag);

                inline for (ti.fields) |f| {
                    if (value == @field(TT, f.name))
                        return try self.encodeAllowVoid(@field(value, f.name));
                }
            } else @compileError("only tagged unions are supported");
        }
    };
}

fn isHashMap(comptime T: type) bool {
    switch (@typeInfo(T)) {
        .@"struct" => {
            // These are the only parts of the HashMap API that are used.
            // `HashMapKeyType` and `HashMapValueType` add further constraints.
            const has1 = @hasDecl(T, "iterator");
            const has2 = @hasDecl(T, "putAssumeCapacity");
            const has3 = @hasDecl(T, "ensureUnusedCapacity");
            const has4 = @hasDecl(T, "Size");
            return has1 and has2 and has3 and has4;
        },
        else => return false,
    }
}

fn HashMapKeyType(comptime T: type) type {
    return HashMapType(T, "key");
}

fn HashMapValueType(comptime T: type) type {
    return HashMapType(T, "value");
}

fn HashMapType(comptime T: type, comptime field_name: []const u8) type {
    const fields = @typeInfo(T.KV).@"struct".fields;

    inline for (fields) |f|
        if (comptime mem.eql(u8, f.name, field_name))
            return f.type;
}

fn isValidHashMapKeyType(comptime T: type) bool {
    return switch (@typeInfo(T)) {
        .int, .float, .bool, .@"enum" => true,
        // Strings are allowed, but we don't quite have strings.
        .pointer => |p| p.size == .Slice,
        else => false,
    };
}

test "round trip union with void member" {
    const U = union(enum) {
        Void: void,
        Hello: u8,
    };
    var buf: [4]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const reader = fbs.reader();
    const writer = fbs.writer();
    var e = encoder(writer);
    try e.encode(U{ .Hello = 123 });
    var d = decoder(std.testing.allocator, reader);
    fbs = io.fixedBufferStream(fbs.getWritten());
    const res = try d.decode(U);
    try std.testing.expectEqual(res, U{ .Hello = 123 });
}
