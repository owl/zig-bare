const std = @import("std");
const io = std.io;
const math = std.math;
const mem = std.mem;
const testing = std.testing;

const bare = @import("bare");
const decoder = bare.decoder;
const encoder = bare.encoder;

const expect = testing.expect;
const expectEqual = testing.expectEqual;
const expectEqualSlices = testing.expectEqualSlices;
const expectError = testing.expectError;

test "refAllDecls" {
    std.testing.refAllDecls(bare);
}

test "read u8" {
    const cases = .{
        .{ "\x00", 0 },
        .{ "\xff", 255 },
        .{ "\x2a", 42 },
    };

    inline for (cases) |pair| {
        var fbs = io.fixedBufferStream(pair[0]);
        const reader = fbs.reader();
        var d = decoder(testing.allocator, reader);
        defer d.deinit();
        try expectEqual(@as(u8, pair[1]), try d.decode(u8));
    }
}

test "read bool" {
    var fbs = io.fixedBufferStream("\x00");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    try expectEqual(d.decode(bool), false);
    fbs = io.fixedBufferStream("\xff");
    try expectError(error.InvalidBool, d.decode(bool));
    fbs = io.fixedBufferStream("\x01");
    try expectEqual(d.decode(bool), true);
}

test "read struct 1" {
    const Foo = struct {
        a: f32,
        b: u8,
        c: bool,
    };

    var fbs = io.fixedBufferStream("\x00\x00\x28\x42\x2a\x01");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const res = try d.decode(Foo);
    try expectEqual(res.a, 42.0);
    try expectEqual(res.b, 42);
    try expectEqual(res.c, true);
}

test "read struct 2" {
    const Foo = struct {
        a: f32,
        b: u8,
        c: bool,
    };

    var fbs = io.fixedBufferStream("\x00\x00\x28\x42\x2a\x00");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const res = try d.decode(Foo);
    try expectEqual(res.a, 42.0);
    try expectEqual(res.b, 42);
    try expectEqual(res.c, false);
}

test "read struct 3" {
    const Foo = struct {
        a: f32,
        b: u8,
        c: bool,
    };

    var fbs = io.fixedBufferStream("\x00\x00\x28\x42\x2b\x00");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const res = try d.decode(Foo);
    try expectEqual(res.a, 42.0);
    try expectEqual(res.b, 43);
    try expectEqual(res.c, false);
}

test "read struct 4" {
    const Foo = struct {
        a: f32,
        b: u8,
        c: bool,
    };

    var fbs = io.fixedBufferStream("\x00\x00\x29\x42\x2b\x00");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const res = try d.decode(Foo);
    try expectEqual(res.a, 42.25);
    try expectEqual(res.b, 43);
    try expectEqual(res.c, false);
}

test "read struct 5" {
    const Foo = struct {
        a: f32,
        b: u8,
        c: bool,
    };

    var fbs = io.fixedBufferStream("\x00\x00\x00\x00\x00\x00");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const res = try d.decode(Foo);
    try expectEqual(res.a, 0.0);
    try expectEqual(res.b, 0);
    try expectEqual(res.c, false);
}

test "read struct 6" {
    const Foo = struct {
        a: f32,
        b: u8,
        c: bool,
    };

    var fbs = io.fixedBufferStream("\xff\xff\xff\xff\xff\x01");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const res = try d.decode(Foo);
    try expect(math.isNan(res.a));
    try expectEqual(res.b, 255);
    try expectEqual(res.c, true);
}

test "read enum" {
    const Foo = enum(u8) {
        a = 2,
        b = 1,
        c = 0,
    };
    var fbs = io.fixedBufferStream("\x02");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    var res = try d.decode(Foo);
    try expectEqual(res, .a);

    fbs = io.fixedBufferStream("\x01");
    res = try d.decode(Foo);
    try expectEqual(res, .b);

    fbs = io.fixedBufferStream("\x00");
    res = try d.decode(Foo);
    try expectEqual(res, .c);
}

test "read invalid enum value" {
    const Foo = enum(u8) {
        a = 2,
        b = 1,
        c = 0,
    };
    var fbs = io.fixedBufferStream("\x03");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    try expectError(error.InvalidEnumTag, d.decode(Foo));
}

test "read optional u8 value" {
    var fbs = io.fixedBufferStream("\x01\x2a");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const res = try d.decode(?u8);
    try expectEqual(res, 42);
}

test "read optional u8 null" {
    var fbs = io.fixedBufferStream("\x00");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const res = try d.decode(?u8);
    try expectEqual(res, null);
}

test "read u8 array" {
    var fbs = io.fixedBufferStream("\x01\x02\x03\x04");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const res = try d.decode([4]u8);
    const expected = [4]u8{ 1, 2, 3, 4 };
    try expectEqual(res, expected);
}

test "read u8 slice" {
    var fbs = io.fixedBufferStream("\x04\x01\x02\x03\x04");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const res = try d.decode([]const u8);
    const expected = [_]u8{ 1, 2, 3, 4 };
    try expectEqualSlices(u8, &expected, res);
}

test "read 0-length slice" {
    var fbs = io.fixedBufferStream("\x00");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const res = try d.decode([]const u8);
    defer testing.allocator.free(res);
    const expected = [_]u8{};
    try expectEqualSlices(u8, &expected, res);
}

test "read map[u8]u8" {
    var fbs = io.fixedBufferStream("\x02\x01\x02\x03\x04");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const map = try d.decode(std.AutoHashMap(u8, u8));
    try expectEqual(map.get(1) orelse unreachable, 2);
    try expectEqual(map.get(3) orelse unreachable, 4);
}

test "read map[u8]u8 with 0 items" {
    var fbs = io.fixedBufferStream("\x00");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const map = try d.decode(std.AutoHashMap(u8, u8));
    try expectEqual(map.count(), 0);
}

test "read map[string]u8" {
    var fbs = io.fixedBufferStream("\x02\x04zomg\x04\x03lol\x02");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const T = std.StringHashMap(u8);
    const map = try d.decode(T);
    try expectEqual(map.get("lol") orelse unreachable, 2);
    try expectEqual(map.get("zomg") orelse unreachable, 4);
}

test "read map overflow" {
    var fbs = io.fixedBufferStream("\xff\xff\xff\xff\xff\xff\xff\xff\xff\x00\x04zomg\x04\x03lol\x02");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const T = std.StringHashMap(u8);
    const map = d.decode(T);
    try expectError(error.Overflow, map);
}

test "read tagged union" {
    const Foo = union(enum) { a: i64, b: bool, c: u8 };
    var fbs = io.fixedBufferStream("\x02\x2a");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const res = try d.decode(Foo);
    try expectEqual(Foo{ .c = 42 }, res);
}

test "write u8" {
    var buf: [1]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const writer = fbs.writer();
    const x: u8 = 42;
    var e = encoder(writer);
    try e.encode(x);
    try expectEqual(fbs.getWritten()[0], x);
}

test "write bool" {
    var buf: [2]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const writer = fbs.writer();
    var e = encoder(writer);
    try e.encode(false);
    try e.encode(true);
    try expectEqual(fbs.getWritten()[0], 0);
    try expectEqual(fbs.getWritten()[1], 1);
}

test "write struct" {
    const Foo = struct {
        a: f32,
        b: u8,
        c: bool,
    };

    var buf: [@sizeOf(Foo)]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const expected = "\x00\x00\x28\x42\x2a\x01";
    const writer = fbs.writer();
    var e = encoder(writer);
    try e.encode(Foo{ .a = 42.0, .b = 42, .c = true });
    try expect(mem.eql(u8, fbs.getWritten(), expected));
}

test "write enum" {
    const Foo = enum(u8) {
        a,
        b,
        c = 2,
    };

    var buf: [@sizeOf(Foo)]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const expected = "\x02";
    const writer = fbs.writer();
    var e = encoder(writer);
    try e.encode(Foo.c);
    try expect(mem.eql(u8, fbs.getWritten(), expected));
}

test "write optional u8 value" {
    var buf: [2]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const writer = fbs.writer();
    const x: ?u8 = 42;
    var e = encoder(writer);
    try e.encode(x);
    try expectEqual(fbs.getWritten()[0], @intFromBool(true));
    try expectEqual(fbs.getWritten()[1], 42);
}

test "write optional u8 null" {
    var buf: [2]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const x: ?u8 = null;
    const writer = fbs.writer();
    var e = encoder(writer);
    try e.encode(x);
    try expectEqual(fbs.getWritten().len, 1);
    try expectEqual(fbs.getWritten()[0], @intFromBool(false));
}

test "write u8 array" {
    var buf: [4]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const x = [4]u8{ 1, 2, 3, 4 };
    const writer = fbs.writer();
    var e = encoder(writer);
    try e.encode(x);
    const expected = [_]u8{ 1, 2, 3, 4 };
    try expectEqual(fbs.getWritten().len, 4);
    try expectEqualSlices(u8, fbs.getWritten(), &expected);
}

test "write slice" {
    var buf: [5]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);

    const expected = [_]u8{ 1, 2, 3, 4 };
    const slajs: []const u8 = &expected;

    const writer = fbs.writer();
    var e = encoder(writer);
    try e.encode(slajs);
    try expectEqual(fbs.getWritten().len, 5);
    try expectEqual(fbs.getWritten()[0], 4);
    try expectEqualSlices(u8, fbs.getWritten()[1..], &expected);
}

test "write map[u8]u8" {
    var buf: [5]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);

    var map = std.AutoHashMap(u8, u8).init(testing.allocator);
    defer map.deinit();

    _ = try map.put(1, 2);
    _ = try map.put(3, 4);

    const writer = fbs.writer();
    var e = encoder(writer);
    try e.encode(map);
    const expected = "\x02\x01\x02\x03\x04";
    try expectEqual(fbs.getWritten().len, 5);
    try expectEqualSlices(u8, fbs.getWritten(), expected);
}

test "write map[string]u8" {
    var buf: [100]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);

    var map = std.StringHashMap(u8).init(testing.allocator);
    defer map.deinit();

    _ = try map.put("lol", 2);
    _ = try map.put("zomg", 4);

    const writer = fbs.writer();
    var e = encoder(writer);
    try e.encode(map);
    const expected = "\x02\x03lol\x02\x04zomg\x04";
    try expectEqual(fbs.getWritten().len, 12);
    try expectEqualSlices(u8, fbs.getWritten(), expected);
}

test "write tagged union" {
    var buf: [10]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);

    const Foo = union(enum) { a: i64, b: bool, c: u8 };
    const foo = Foo{ .a = 42 };

    const writer = fbs.writer();
    var e = encoder(writer);
    try e.encode(foo);
    const expected = "\x00\x2a\x00\x00\x00\x00\x00\x00\x00";
    try expectEqual(fbs.getWritten().len, 1 + @sizeOf(i64));
    try expectEqualSlices(u8, fbs.getWritten(), expected);
}

test "write tagged union with void member active" {
    var buf: [10]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);

    const Foo = union(enum) { a: i64, b: void, c: u8 };
    const foo = Foo{ .b = {} };

    const writer = fbs.writer();
    var e = encoder(writer);
    try e.encode(foo);
    const expected = "\x01";
    try expectEqualSlices(u8, fbs.getWritten(), expected);
}

test "round trip u8" {
    var buf: [0x100]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const reader = fbs.reader();
    const writer = fbs.writer();
    const x: u8 = 42;
    var e = encoder(writer);
    try e.encode(x);
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    fbs = io.fixedBufferStream(fbs.getWritten());
    const res = try d.decode(u8);
    try expectEqual(res, x);
}

test "round trip struct" {
    const Foo = struct {
        a: f32,
        b: u8,
        c: bool,
    };

    const foo = Foo{ .a = 42.0, .b = 3, .c = true };
    var buf: [@sizeOf(Foo)]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const reader = fbs.reader();
    const writer = fbs.writer();
    var e = encoder(writer);
    try e.encode(foo);
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    fbs = io.fixedBufferStream(fbs.getWritten());
    const res = try d.decode(Foo);
    try expectEqual(res, foo);
}

test "round trip enum" {
    const Foo = enum(u8) {
        a,
        b,
        c = 2,
    };

    var buf: [@sizeOf(Foo)]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const reader = fbs.reader();
    const writer = fbs.writer();
    var e = encoder(writer);
    try e.encode(Foo.c);
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    fbs = io.fixedBufferStream(fbs.getWritten());
    const res = try d.decode(Foo);
    try expectEqual(res, Foo.c);
}

test "round trip i64 array" {
    var buf: [0x100]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const reader = fbs.reader();
    const writer = fbs.writer();
    const arr = [_]i64{ -1, 1, math.maxInt(i64), math.minInt(i64) };
    var e = encoder(writer);
    try e.encode(arr);
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    fbs = io.fixedBufferStream(fbs.getWritten());
    const res = try d.decode(@TypeOf(arr));
    try expectEqualSlices(i64, &arr, &res);
}

test "round trip i32 slice" {
    var buf: [0x100]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const reader = fbs.reader();
    const writer = fbs.writer();

    var e = encoder(writer);

    const expected = [4]i32{ -1, 2, math.maxInt(i32), math.minInt(i32) };
    const slajs: []const i32 = &expected;

    try e.encode(slajs);
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    fbs = io.fixedBufferStream(fbs.getWritten());
    const res = try d.decode([]const i32);
    try expectEqualSlices(i32, slajs, res);
}

test "round trip tagged union" {
    const Foo = union(enum) {
        a: i64,
        b: u32,
        c: bool,
    };
    const foo = Foo{ .b = 42 };
    var buf: [0x100]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const reader = fbs.reader();
    const writer = fbs.writer();

    var e = encoder(writer);
    try e.encode(foo);
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    fbs = io.fixedBufferStream(fbs.getWritten());
    const res = try d.decode(Foo);

    try expectEqual(foo, res);
}

test "round trip tagged union with void member active" {
    const Foo = union(enum) {
        a: i64,
        b: void,
        c: bool,
    };
    const foo = Foo{ .b = {} };
    var buf: [0x100]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const reader = fbs.reader();
    const writer = fbs.writer();

    var e = encoder(writer);
    try e.encode(foo);
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    fbs = io.fixedBufferStream(fbs.getWritten());
    const res = try d.decode(Foo);

    try expectEqual(foo, res);
}

test "invalid union returns error" {
    const Foo = union(enum) { a: i64, b: bool, c: u8 };
    var fbs = io.fixedBufferStream("\x09\x2a");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    const res = d.decode(Foo);
    try expectError(error.InvalidUnion, res);
}

test "bare.Decoder frees its memory" {
    var fbs = io.fixedBufferStream("\x02\x01\x02\x03\x04");
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    _ = try d.decode(std.AutoHashMap(u8, u8));
    fbs = io.fixedBufferStream("\x04\x01\x02\x03\x04");
    _ = try d.decode([]const u8);
    // testing.allocator will yell if memory is leaked
}
