const builtin = @import("builtin");
const std = @import("std");
const mem = std.mem;
const fs = std.fs;
const Target = std.build.Target;
const program_name = "bare";
const sep = fs.path.sep_str;

pub fn build(b: *std.Build) !void {
    const optimize = b.standardOptimizeOption(.{});
    const target = b.standardTargetOptions(.{});

    const varint_dep = b.dependency("varint", .{});

    const bare_mod = b.addModule("bare", .{
        .root_source_file = b.path(b.pathJoin(&.{ "src", "bare.zig" })),
        .imports = &.{
            .{
                .name = "varint",
                .module = varint_dep.module("varint"),
            },
        },
    });

    const bench = b.addExecutable(.{
        .name = program_name ++ "-benchmark",
        .root_source_file = b.path(b.pathJoin(&.{ "src", "benchmark.zig" })),
        .optimize = optimize,
        .target = target,
    });

    bench.root_module.addImport("bare", bare_mod);
    bench.root_module.addImport("varint", varint_dep.module("varint"));

    const unit_tests = b.addTest(.{
        .root_source_file = b.path(b.pathJoin(&.{ "src", "test.zig" })),
        .optimize = optimize,
        .target = target,
    });

    unit_tests.root_module.addImport("bare", bare_mod);

    const interop_tests = b.addTest(.{
        .root_source_file = b.path(b.pathJoin(&.{ "interop", "root.zig" })),
        .optimize = optimize,
        .target = target,
    });

    interop_tests.root_module.addImport("bare", bare_mod);

    const bench_run_cmd = b.addRunArtifact(bench);
    const bench_run_step = b.step("bench", "Run " ++ program_name ++ " benchmarks");

    bench_run_step.dependOn(&bench_run_cmd.step);

    const test_step = b.step("test", "Run all " ++ program_name ++ " tests");

    const run_unit_tests = b.addRunArtifact(unit_tests);
    test_step.dependOn(&run_unit_tests.step);

    const run_interop_tests = b.addRunArtifact(interop_tests);
    test_step.dependOn(&run_interop_tests.step);
}
