# zig-bare

[BARE Message Encoding](https://baremessages.org/) implementation in Zig.

[![builds.sr.ht status](https://builds.sr.ht/~alva/zig-bare/commits/trunk.svg)](https://builds.sr.ht/~alva/zig-bare?)

## Building

The `trunk` branch is tested against Zig `master` branch on build.

```bash
# Use -Doptimize=Release{Fast, Safe, Small} for release build.
zig build

# Run tests.
zig build test
```

## Usage

Use [the Zig build system](https://ziglang.org/download/0.11.0/release-notes.html#Package-Management)
to add the `"bare"` module to your project.

Declare your types as usual, then use `Encoder` to encode them,
and `Decoder` to decode them:

```zig
const Foo = union(enum) {
    x: u64,
    y: []const u8,
    z: void,
};

const Bar = struct {
    a: f32,
    b: u8,
    c: Foo,
};

const x = Bar{
    .a = 3.14,
    .b = 2,
    .c = .{ .y = "hello" },
};

test "example" {
    var buf: [12]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);

    const reader = fbs.reader();
    const writer = fbs.writer();

    var e = encoder(writer);
    try e.encode(x);

    try fbs.seekTo(0);

    var d = decoder(std.testing.allocator, reader);
    defer d.deinit();

    const y = try d.decode(Bar);

    std.log.info("x: {}", .{x});
    std.log.info("y: {}", .{y});
}
```

Hash maps and slices require allocation;
call `deinit` on a `Decoder` to free any memory that was allocated.

## BARE conformance

All BARE types are supported, although `data<length>` and `data` are the same
as `[length]u8` and `[]u8`, and `string` is also the same as `[]u8`.

`usize` and `c_uint` are encoded as variable-length unsigned integers,
and `isize` and `c_int` are encoded as variable-length signed integers.

For `map[type A]type B`, `std.HashMap` is used.

When using `Encoder`, all BARE invariants are enforced at compile-time.

### Interoperability

There are tests ported from go-bare in an attempt to ensure interoperability.
Please report any interoperability issues you find.

If you can port relevant test cases, that would also be very welcome. :)

## Code generation

There is no schema parser or code generation.

I don't need it myself, but I would gladly accept contributions.

## Contributing

Send patches and questions to
[~alva/zig-bare@lists.sr.ht](mailto:~alva/zig-bare@lists.sr.ht).
