const std = @import("std");
const io = std.io;
const math = std.math;
const mem = std.mem;
const testing = std.testing;

const bare = @import("bare");
const decoder = bare.decoder;
const encoder = bare.encoder;

const expect = testing.expect;
const expectEqual = testing.expectEqual;
const expectEqualSlices = testing.expectEqualSlices;
const expectError = testing.expectError;

const payloads1 = [_][]const u8{
    &[_]u8{0x42},
    &[_]u8{ 0xFE, 0xCA },
    &[_]u8{ 0xEF, 0xBE, 0xAD, 0xDE },
    &[_]u8{ 0xEF, 0xBE, 0xAD, 0xDE, 0xBE, 0xBA, 0xFE, 0xCA },
    &[_]u8{ 0xEF, 0xFD, 0xB6, 0xF5, 0x0D },
    &[_]u8{0xD6},
    &[_]u8{ 0x2E, 0xFB },
    &[_]u8{ 0xB2, 0x9E, 0x43, 0xFF },
    &[_]u8{ 0x4F, 0x0B, 0x6E, 0x9D, 0xAB, 0x23, 0xD4, 0xFF },
    &[_]u8{ 0x9B, 0x85, 0xE3, 0x0B },
    &[_]u8{ 0x71, 0x2D, 0xA7, 0x44 },
    &[_]u8{ 0x9B, 0x6C, 0xC9, 0x20, 0xF0, 0x21, 0x3F, 0x42 },
    &[_]u8{0x01},
    &[_]u8{0x00},
    &[_]u8{ 0x1B, 0xE3, 0x81, 0x93, 0xE3, 0x82, 0x93, 0xE3, 0x81, 0xAB, 0xE3, 0x81, 0xA1, 0xE3, 0x81, 0xAF, 0xE3, 0x80, 0x81, 0xE4, 0xB8, 0x96, 0xE7, 0x95, 0x8C, 0xEF, 0xBC, 0x81 },
};

// tests ported from go-bare
test "marshal value" {
    inline for (.{
        .{ payloads1[0], u8, 0x42 },
        .{ payloads1[1], u16, 0xCAFE },
        .{ payloads1[2], u32, 0xDEADBEEF },
        .{ payloads1[3], u64, 0xCAFEBABEDEADBEEF },
        .{ payloads1[4], usize, 0xDEADBEEF },
        .{ payloads1[5], i8, -42 },
        .{ payloads1[6], i16, -1234 },
        .{ payloads1[7], i32, -12345678 },
        .{ payloads1[8], i64, -12345678987654321 },
        .{ payloads1[9], isize, -12345678 },
        .{ payloads1[10], f32, 1337.42 },
        .{ payloads1[11], f64, 133713371337.42424242 },
        .{ payloads1[12], bool, true },
        .{ payloads1[13], bool, false },
        .{ payloads1[14], []const u8, "こんにちは、世界！" },
    }) |tuple| {
        var buf: [0x1_000]u8 = undefined;
        var fbs = io.fixedBufferStream(&buf);
        const writer = fbs.writer();
        var e = encoder(writer);
        // should return without error
        const val: tuple[1] = tuple[2];
        try e.encode(val);
        try expectEqualSlices(u8, tuple[0], fbs.getWritten());
    }
}

test "marshal optional" {
    {
        var buf: [0x1_000]u8 = undefined;
        var fbs = io.fixedBufferStream(&buf);
        const writer = fbs.writer();
        var e = encoder(writer);

        const val: ?u32 = null;
        try e.encode(val);
        try expectEqualSlices(u8, &[_]u8{0x00}, fbs.getWritten());
    }
    {
        var buf: [0x1_000]u8 = undefined;
        var fbs = io.fixedBufferStream(&buf);
        const writer = fbs.writer();
        var e = encoder(writer);
        const val: ?u32 = 0xDEADBEEF;

        try e.encode(val);
        try testing.expectEqualSlices(u8, &[_]u8{ 0x01, 0xEF, 0xBE, 0xAD, 0xDE }, fbs.getWritten());
    }
}

test "marshal struct" {
    var buf: [0x1_000]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const writer = fbs.writer();
    var e = encoder(writer);

    const Coordinates = struct { x: usize, y: usize, z: usize };
    const coords = Coordinates{ .x = 1, .y = 2, .z = 3 };
    const reference = [_]u8{ 0x01, 0x02, 0x03 };

    try e.encode(coords);
    try testing.expectEqualSlices(u8, &reference, fbs.getWritten());
}

test "marshal array" {
    var buf: [0x1_000]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const writer = fbs.writer();
    var e = encoder(writer);

    const val = [4]u8{ 0x11, 0x22, 0x33, 0x44 };
    const reference = &[_]u8{ 0x11, 0x22, 0x33, 0x44 };
    try e.encode(val);
    try testing.expectEqualSlices(u8, reference, fbs.getWritten());
}

test "marshal slice" {
    var buf: [0x1_000]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const writer = fbs.writer();
    var e = encoder(writer);

    const val: []const u8 = &[_]u8{ 0x11, 0x22, 0x33, 0x44 };
    const reference = &[_]u8{ 0x04, 0x11, 0x22, 0x33, 0x44 };
    try e.encode(val);
    try testing.expectEqualSlices(u8, reference, fbs.getWritten());
}

test "marshal map" {
    var buf: [0x1_000]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const writer = fbs.writer();
    var e = encoder(writer);

    var map = std.AutoHashMap(u8, u8).init(testing.allocator);
    defer map.deinit();
    try map.put(0x01, 0x11);
    try map.put(0x01, 0x11);
    try map.put(0x02, 0x22);
    try map.put(0x03, 0x33);

    try e.encode(map);

    const references = &[_][]const u8{
        &[_]u8{
            0x03,
            0x01,
            0x11,
            0x02,
            0x22,
            0x03,
            0x33,
        },
        &[_]u8{
            0x03,
            0x01,
            0x11,
            0x03,
            0x33,
            0x02,
            0x22,
        },
        &[_]u8{
            0x03,
            0x03,
            0x33,
            0x02,
            0x22,
            0x01,
            0x11,
        },
        &[_]u8{
            0x03,
            0x01,
            0x11,
            0x03,
            0x33,
            0x02,
            0x22,
        },
        &[_]u8{
            0x03,
            0x03,
            0x33,
            0x01,
            0x11,
            0x02,
            0x22,
        },
        &[_]u8{
            0x03,
            0x02,
            0x22,
            0x03,
            0x33,
            0x01,
            0x11,
        },
        &[_]u8{
            0x03,
            0x02,
            0x22,
            0x01,
            0x11,
            0x03,
            0x33,
        },
    };

    for (references) |ref| {
        if (std.mem.eql(u8, ref, fbs.getWritten()))
            return;
    }

    unreachable;
}

test "marshal union" {
    const NameAge = union(enum) {
        name: []const u8,
        age: isize,
    };

    var buf: [0x1_000]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const writer = fbs.writer();
    var e = encoder(writer);

    {
        const val = NameAge{ .name = "Mary" };
        try e.encode(val);
        const reference = &[_]u8{ 0x00, 0x04, 0x4d, 0x61, 0x72, 0x79 };
        try testing.expectEqualSlices(u8, reference, fbs.getWritten());
    }

    fbs.reset();

    {
        const val = NameAge{ .age = 24 };
        try e.encode(val);
        const reference = &[_]u8{ 0x01, 0x30 };
        try testing.expectEqualSlices(u8, reference, fbs.getWritten());
    }
}
