const std = @import("std");
const bare = @import("bare");

const string = []const u8;
const MetaData = std.StringHashMap([]const u8);

const PublicKey = [128]u8;
const Time = string; // ISO 8601 string

const Department = enum(usize) {
    ACCOUNTING,
    ADMINISTRATION,
    CUSTOMER_SERVICE,
    DEVELOPMENT,

    // Reserved for the CEO
    JSMITH = 99,
};

const Customer = struct {
    name: string,
    email: string,
    address: Address,
    orders: []struct {
        orderId: i64,
        quantity: i32,
    },
    metadata: MetaData,
};

const Employee = struct {
    name: string,
    email: string,
    address: Address,
    department: Department,
    hireDate: Time,
    publicKey: ?PublicKey,
    metadata: MetaData,
};

const TerminatedEmployee = void;

const Person = union(enum) {
    customer: Customer,
    employee: Employee,
    terminated_employee: TerminatedEmployee,
};

const Address = struct {
    address: [4]string,
    city: string,
    state: string,
    country: string,
};

const testing = std.testing;

test "decode customer.bin" {
    const customer = @embedFile("customer.bin");
    var fbs = std.io.fixedBufferStream(customer);

    const reader = fbs.reader();
    var d = bare.decoder(testing.allocator, reader);
    defer d.deinit();
    const res = try d.decode(Person);

    switch (res) {
        .customer => |c| {
            try testing.expectEqualSlices(u8, "James Smith", c.name);
            try testing.expectEqualSlices(u8, "jsmith@example.org", c.email);
        },
        else => unreachable,
    }
}

test "decode employee.bin" {
    const customer = @embedFile("employee.bin");
    var fbs = std.io.fixedBufferStream(customer);

    const reader = fbs.reader();
    var d = bare.decoder(testing.allocator, reader);
    defer d.deinit();
    const res = try d.decode(Person);

    switch (res) {
        .employee => |c| {
            try testing.expectEqualSlices(u8, "Tiffany Doe", c.name);
            try testing.expectEqualSlices(u8, "tiffanyd@acme.corp", c.email);
        },
        else => unreachable,
    }
}

test "decode terminated.bin" {
    const customer = @embedFile("terminated.bin");
    var fbs = std.io.fixedBufferStream(customer);

    const reader = fbs.reader();
    var d = bare.decoder(testing.allocator, reader);
    defer d.deinit();
    const res = try d.decode(Person);

    switch (res) {
        .terminated_employee => |t| {
            try testing.expectEqual(t, {});
        },
        else => unreachable,
    }
}

test "decode people.bin" {
    const customer = @embedFile("people.bin");
    var fbs = std.io.fixedBufferStream(customer);

    const reader = fbs.reader();
    var d = bare.decoder(testing.allocator, reader);
    defer d.deinit();
    var res = try d.decode(Person);

    switch (res) {
        .customer => {},
        else => unreachable,
    }

    res = try d.decode(Person);

    switch (res) {
        .employee => {},
        else => unreachable,
    }

    res = try d.decode(Person);

    switch (res) {
        .terminated_employee => {},
        else => unreachable,
    }
}
