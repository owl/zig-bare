const std = @import("std");
const io = std.io;
const math = std.math;
const mem = std.mem;
const testing = std.testing;

const bare = @import("bare");
const decoder = bare.decoder;
const encoder = bare.encoder;

const expect = testing.expect;
const expectEqual = testing.expectEqual;
const expectEqualSlices = testing.expectEqualSlices;
const expectError = testing.expectError;

const payloads1 = [_][]const u8{
    &[_]u8{0x42},
    &[_]u8{ 0xFE, 0xCA },
    &[_]u8{ 0xEF, 0xBE, 0xAD, 0xDE },
    &[_]u8{ 0xEF, 0xBE, 0xAD, 0xDE, 0xBE, 0xBA, 0xFE, 0xCA },
    &[_]u8{ 0xEF, 0xFD, 0xB6, 0xF5, 0x0D },
    &[_]u8{0xD6},
    &[_]u8{ 0x2E, 0xFB },
    &[_]u8{ 0xB2, 0x9E, 0x43, 0xFF },
    &[_]u8{ 0x4F, 0x0B, 0x6E, 0x9D, 0xAB, 0x23, 0xD4, 0xFF },
    &[_]u8{ 0x9B, 0x85, 0xE3, 0x0B },
    &[_]u8{ 0x71, 0x2D, 0xA7, 0x44 },
    &[_]u8{ 0x9B, 0x6C, 0xC9, 0x20, 0xF0, 0x21, 0x3F, 0x42 },
    &[_]u8{ 0x00, 0x01, 0x02 },
    &[_]u8{ 0x1B, 0xE3, 0x81, 0x93, 0xE3, 0x82, 0x93, 0xE3, 0x81, 0xAB, 0xE3, 0x81, 0xA1, 0xE3, 0x81, 0xAF, 0xE3, 0x80, 0x81, 0xE4, 0xB8, 0x96, 0xE7, 0x95, 0x8C, 0xEF, 0xBC, 0x81 },
};

// tests ported from go-bare
test "unmarshal value" {
    inline for (.{
        .{ payloads1[0], u8, 0x42 },
        .{ payloads1[1], u16, 0xCAFE },
        .{ payloads1[2], u32, 0xDEADBEEF },
        .{ payloads1[3], u64, 0xCAFEBABEDEADBEEF },
        .{ payloads1[4], c_uint, 0xDEADBEEF },
        .{ payloads1[4], usize, 0xDEADBEEF },
        .{ payloads1[5], i8, -42 },
        .{ payloads1[6], i16, -1234 },
        .{ payloads1[7], i32, -12345678 },
        .{ payloads1[8], i64, -12345678987654321 },
        .{ payloads1[9], c_int, -12345678 },
        .{ payloads1[9], isize, -12345678 },
        .{ payloads1[10], f32, 1337.42 },
        .{ payloads1[11], f64, 133713371337.42424242 },
        .{ payloads1[12][0..], bool, false },
        .{ payloads1[12][1..], bool, true },
        .{ payloads1[12][2..], bool, error.InvalidBool },
        .{ payloads1[13], []const u8, "こんにちは、世界！" },
    }) |tuple| {
        var fbs = io.fixedBufferStream(tuple[0]);
        const reader = fbs.reader();
        var d = decoder(testing.allocator, reader);
        defer d.deinit();
        const res = switch (@typeInfo(@TypeOf(tuple[2]))) {
            .error_set => |_| {
                try testing.expectError(tuple[2], d.decode(tuple[1]));
                continue;
            },
            else => try d.decode(tuple[1]),
        };
        try testing.expectEqualDeep(@as(tuple[1], tuple[2]), res); // catch {
        //    try std.io.getStdErr().writer().print("{any}", .{tuple});
        //};
    }
}

test "unmarshal optional" {
    {
        const payload = &[_]u8{0x00};
        var fbs = io.fixedBufferStream(payload);
        const reader = fbs.reader();
        var d = decoder(testing.allocator, reader);
        defer d.deinit();
        const val = try d.decode(?i32);
        try expectEqual(@as(?i32, null), val);
    }

    {
        const payload = &[_]u8{ 0x01, 0xEF, 0xBE, 0xAD, 0xDE };
        var fbs = io.fixedBufferStream(payload);
        const reader = fbs.reader();
        var d = decoder(testing.allocator, reader);
        defer d.deinit();
        const val = try d.decode(?u32);
        try expectEqual(@as(?u32, 0xDEADBEEF), val);
    }
}

test "unmarshal struct" {
    const Coordinates = struct { x: usize, y: usize, z: usize };
    const payload = &[_]u8{ 0x01, 0x02, 0x03 };
    var fbs = io.fixedBufferStream(payload);
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const coords = try d.decode(Coordinates);
    try expectEqual(@as(usize, 1), coords.x);
    try expectEqual(@as(usize, 2), coords.y);
    try expectEqual(@as(usize, 3), coords.z);
}

test "unmarshal omitted fields" {
    const Coordinates = struct {
        x: usize,
        y: usize,
        // TODO: we don't have omitted fields, just optional.
        //z: ?usize,
    };
    const payload = &[_]u8{ 0x01, 0x02 };
    var fbs = io.fixedBufferStream(payload);
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const coords = try d.decode(Coordinates);
    try expectEqual(@as(usize, 1), coords.x);
    try expectEqual(@as(usize, 2), coords.y);
}

test "unmarshal array" {
    const payload = &[_]u8{ 0x11, 0x22, 0x33, 0x44 };
    var fbs = io.fixedBufferStream(payload);
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const val = try d.decode([4]u8);
    try expectEqual(@as(usize, 0x11), val[0]);
    try expectEqual(@as(usize, 0x22), val[1]);
    try expectEqual(@as(usize, 0x33), val[2]);
    try expectEqual(@as(usize, 0x44), val[3]);
}

test "unmarshal slice" {
    const payload = &[_]u8{ 0x04, 0x11, 0x22, 0x33, 0x44 };
    var fbs = io.fixedBufferStream(payload);
    const reader = fbs.reader();
    var d = decoder(testing.allocator, reader);
    defer d.deinit();
    const val = try d.decode([]const u8);
    try expectEqual(@as(usize, 0x11), val[0]);
    try expectEqual(@as(usize, 0x22), val[1]);
    try expectEqual(@as(usize, 0x33), val[2]);
    try expectEqual(@as(usize, 0x44), val[3]);
}

test "unmarshal map" {
    {
        const payload = &[_]u8{
            0x03,
            0x01,
            0x11,
            0x02,
            0x22,
            0x03,
            0x33,
        };
        var fbs = io.fixedBufferStream(payload);
        const reader = fbs.reader();
        var d = decoder(testing.allocator, reader);
        defer d.deinit();
        const val = try d.decode(std.AutoHashMap(u8, u8));
        try expectEqual(@as(usize, 3), val.count());
        try expectEqual(@as(?u8, 0x11), val.get(0x01));
        try expectEqual(@as(?u8, 0x22), val.get(0x02));
        try expectEqual(@as(?u8, 0x33), val.get(0x03));
    }

    // rejects duplicate keys
    {
        const payload = &[_]u8{
            0x02,
            0x01,
            0x13,
            0x01,
            0x37,
        };
        var fbs = io.fixedBufferStream(payload);
        const reader = fbs.reader();
        var d = decoder(testing.allocator, reader);
        defer d.deinit();
        const val = d.decode(std.AutoHashMap(u8, u8));
        try expectError(error.DuplicateKey, val);
    }

    // respects size limits
    {
        // TODO: we don't have such limits.
    }
}

test "unmarshal union" {
    const NameAge = union(enum) {
        name: []const u8,
        age: isize,
    };

    {
        const payload = &[_]u8{ 0x00, 0x04, 0x4d, 0x61, 0x72, 0x79 };
        var fbs = io.fixedBufferStream(payload);
        const reader = fbs.reader();
        var d = decoder(testing.allocator, reader);
        defer d.deinit();
        const val = try d.decode(NameAge);
        try expectEqualSlices(u8, (NameAge{ .name = "Mary" }).name, val.name);
    }

    {
        const payload = &[_]u8{ 0x01, 0x30 };
        var fbs = io.fixedBufferStream(payload);
        const reader = fbs.reader();
        var d = decoder(testing.allocator, reader);
        defer d.deinit();
        const val = try d.decode(NameAge);
        try expectEqual((NameAge{ .age = 24 }).age, val.age);
    }

    {
        const payload = &[_]u8{ 0x13, 0x37 };
        var fbs = io.fixedBufferStream(payload);
        const reader = fbs.reader();
        var d = decoder(testing.allocator, reader);
        defer d.deinit();
        const val = d.decode(NameAge);
        try expectError(error.InvalidUnion, val);
    }
}

test "unmarshal custom" {
    // TODO: we should allow custom decode methods.
}
