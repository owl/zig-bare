const std = @import("std");

test {
    _ = @import("go-bare/unmarshal_test.zig");
    _ = @import("go-bare/marshal_test.zig");
    _ = @import("go-bare/examples/schema.zig");
}
